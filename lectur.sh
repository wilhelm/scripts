#!/bin/bash

cd '/var/www/html/lecturaddicts/'
gnome-terminal --maximize --tab -e 'bash -c "git status;$SHELL"'
tmux kill-session -t lecturaddicts
tmux new-session -d -s lecturaddicts
tmux split-window -d -t lecturaddicts -h
# we have two windows one next to the other

tmux send-keys -t 1 'cd serveur/ && python manage.py runserver' enter
tmux send-keys -t 0 'cd client/ && grunt serve' enter
tmux attach-session -t lecturaddicts
