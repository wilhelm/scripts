#!/bin/bash

tmux new-session -d -s PSC
tmux split-window -d -t PSC -v
# we have two windows one above the other

tmux split-window -d -t PSC -h
tmux split-window -d -t 2 -h
# we have 4 windows

tmux split-window -d -t 0 -v
tmux split-window -d -t 2 -v
tmux split-window -d -t 4 -v
tmux split-window -d -t 6 -v
# we have 8 windows

for i in {0..14..2}
do
    tmux split-window -d -t $i -h
done
tmux send-keys -t 0 'cd Serveur/' enter
tmux send-keys -t 0 'make start' enter
sleep 2
#tmux attach-session -t PSC
for i in {1..15}
do
    tmux send-keys -t $i 'cd Client' enter
    tmux send-keys -t $i 'make start' enter
    sleep 1
done
echo "Clients lancés"
#tmux attach-session -t PSC
for i in {1..15}
do
    tmux send-keys -t $i 'clement' enter
done
sleep 3
echo "Début de connexion"
#tmux attach-session -t PSC
for i in {1..15}
do
    tmux send-keys -t $i 'clement' enter
    sleep 1
done
echo "Fin de connexion"
#tmux attach-session -t PSC
for i in {1..15}
do
    tmux send-keys -t $i "traceroute $i" enter
done
tmux attach-session -t PSC
