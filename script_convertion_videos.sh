#!/bin/bash
convertion(){
    if [ $# -gt 1 ]
    then
        dossierparent=$2
    else
        dossierparent=$1
    fi
    echo $dossierparent
    cd $1
    `rename 's/[[:blank:]]/_/g' *`
    for fichier in `ls`
    do
        if [ -d $fichier ] #si on a un dossier
        then #on convertit tout
            `rename 's/[[:blank:]]/_/g' *`
            echo "on a un dossier : $fichier "
            if [ $fichier != 'apercu' ]&&[ $fichier != 'ogv' ]&&[ $fichier != 'mp4' ]&&[ $fichier != 'webm' ]
            then
                echo "on va le traiter"
                cd $fichier
                execPath=`pwd`
                echo "appel récursif sur un sous-dossier : $execPath"
                convertion $execPath $dossierparent
            fi
        elif [ -f $fichier ] #si on a un fichier
        then
            echo "on a un fichier : $fichier"
            for ext in 'mp4' 'ogv' 'webm'
            do
                echo $dossierparent'/'$ext'/'${fichier%%.*}'.'$ext
                if [ ! -f $dossierparent'/'$ext'/'${fichier%%.*}'.'$ext ] #si les elements convertis existent déjà
                then
                    if [ ${fichier#*.} = $ext ]
                    then
                        echo "le fichier existe déjà : on le déplace"
                        echo "cp $fichier $dossierparent/$ext/${fichier%%.*}.$ext"
                        `cp $fichier $dossierparent/$ext/${fichier%%.*}.$ext`
                    else
                        echo "le fichier n'est pas encore converti : $ext"
                        echo "ffmpeg -i $fichier $dossierparent/$ext/${fichier%%.*}.$ext"
                        `ffmpeg -i $fichier $dossierparent/$ext/${fichier%%.*}.$ext`
                    fi
                fi
            done
            echo $dossierparent'/apercu/'${fichier%%.*}'.jpg'
            if [ ! -f $dossierparent'/apercu/'${fichier%%.*}'.jpg' ] #si les elements convertis existent déjà
            then
                echo "le fichier n'est pas encore converti : jpg"
                echo "ffmpeg -i $fichier -vcodec mjpeg -vframes 1 -an -f rawvideo -s 250x250 -ss 10 $dossierparent/apercu/${fichier%%.*}.jpg"
                `ffmpeg -i $fichier -vcodec mjpeg -vframes 1 -an -f rawvideo -s 250x250 -ss 10 $dossierparent/apercu/${fichier%%.*}.jpg`
            fi
            #on supprime le fichier pour ne pas le ravoir dans les pattes
            echo "rm $fichier"
            #`rm $fichier`
        fi
    done
}
convertion $*
