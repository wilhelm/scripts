#!/bin/bash

cd '/var/www/html/Sigma/'
gnome-terminal --maximize --tab -e 'zsh -c "tmux kill-session -t sigma-git; tmux new-session -d -s sigma-git; tmux split-window -d -t sigma-git -h; tmux send-keys -t sigma-git:0.0 \"cd backend/ && git status\" enter; tmux send-keys -t sigma-git:0.1 \"cd frontend && git status\" enter; tmux attach-session -t sigma-git;$SHELL"'
tmux kill-session -t sigma
tmux new-session -d -s sigma
tmux split-window -d -t sigma -h
# we have two windows one next to the other

tmux send-keys -t sigma:0.0 'cd backend/ && source .env/bin/activate && ./manage.py runserver' enter
tmux send-keys -t sigma:0.1 'cd frontend/ && npm start' enter
tmux attach-session -t sigma
