#!/bin/sh

echo -n " Bonjour. Pour configurer tes paramètres de connexion, merci de dire si tu es à l'X ou à l'extérieur (x/Ext):"
lieu=ext
read lieu
ok=1
until [ "$ok" = 0 ]
do
    case "${lieu:-ext}" in
        x | X )
            bash ~/inX.sh
            echo "\n Bienvenue à l'X, tes paramètres sont maintenant configurés. Bon travail\n" &
            sleep 1 &
            ok=0;;
        ext | Ext | e )
            bash ~/extX.sh
            echo "\n Tes paramètres sont bien configurés. Bon travail\n" &
            sleep 1 &
            ok=0;;
        * )
            echo -n "\n Merci de taper x si tu es à l'X et e sinon: "
            read lieu;;
    esac
done
exit 0
