#!/bin/bash

#Met le bon fichier de config ssh
cp ~/.ssh/configX ~/.ssh/config

#Met le bon fichier apt.conf
[ ! -e /etc/apt/apt.conf ] && sudo cp /etc/apt/apt.confX /etc/apt/apt.conf

#Prend la bonne connexion filaire
nmcli con up id "X"
