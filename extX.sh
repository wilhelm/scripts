#!/bin/bash

#Met le bon fichier de config ssh
cp ~/.ssh/configNormale ~/.ssh/config

#Met le bon fichier apt.conf
[ -e /etc/apt/apt.conf ] && sudo rm /etc/apt/apt.conf

#Prend la bonne connexion filaire
nmcli con up id "Normal"
