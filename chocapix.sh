#!/bin/bash

cd '/var/www/html/Chocapix/'
gnome-terminal --maximize --tab -e 'zsh -c "tmux kill-session -t chocapix-git; tmux new-session -d -s chocapix-git; tmux split-window -d -t chocapix-git -h; tmux send-keys -t chocapix-git:0.0 \"cd chocapix-server/ && git status\" enter; tmux send-keys -t chocapix-git:0.1 \"cd chocapix-client && git status\" enter; tmux attach-session -t chocapix-git;$SHELL"'

tmux kill-session -t chocapix
tmux new-session -d -s chocapix
tmux split-window -d -t chocapix -h
# we have two windows one next to the other

tmux send-keys -t chocapix:0.1 'cd chocapix-server/ && python manage.py runserver' enter
tmux send-keys -t chocapix:0.0 'cd chocapix-client/ && grunt serve' enter
tmux attach-session -t chocapix
