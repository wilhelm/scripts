#!/bin/bash
convertion(){
    liste $1 $2
    remplacement $*
}
liste() {
    for file in `ls $1 |grep Ps | grep -v PsClient`
    do
        echo ${file%%.*} >> $2
    done
}
remplacement() {
    dossierparent=$3
    fichier2=`cat $2`
    cd $3
    i=3
    j=8
    k=9
    for fichier in $fichier2 #pour chaque classe
    do
        #echo $fichier
        # php
        for file in  `find . -name 'PsClient*.php' -and -not -name 'PsClient.php'` #pour chaque élément s'appelant PsClient___.php
        do
            nv_fichier=${file/'PsClient'/$fichier}
            cp $file $nv_fichier #copier coller le fichier
            sed -i "s/psclient/${fichier,,}/g" $nv_fichier #remplacer psclient par le nom de classe en minuscules
            sed -i "s/PsClient/$fichier/g" $nv_fichier #remplacer PsClient par le nom de classe
        done
        # html
        nfile=PsClientRest
        nfile="Resources/views/${nfile/PsClient/$fichier}"
        cp -R "Resources/views/PsClientRest" "$nfile"
        sed -i "s/psclient/${fichier,,}/g" "$nfile/new.html.twig"
        sed -i "s/PsClient/$fichier/g" "$nfile/new.html.twig"
        # yml
        #services
        src_1='Resources/config/services.yml'
        sed -n -e "1,$i""p" $src_1 >> "$src_1.new"
        lignes=`sed -n -e "2,3p" $src_1`
        lignes="${lignes//PsClient/$fichier}"
        lignes="${lignes//psclient/${fichier,,}}"
        echo "$lignes" >> "$src_1.new"
        sed -n -e "$(($i+1)),$(($k-1))""p" $src_1 >> "$src_1.new"
        lignes=`sed -n -e "$(($j-2)),$j""p" $src_1`
        lignes="${lignes//PsClient/$fichier}"
        lignes="${lignes//psclient/${fichier,,}}"
        echo "$lignes" >> "$src_1.new"
        sed -n -e "$k,$""p" $src_1 >> "$src_1.new"
        mv "$src_1.new" $src_1
        i=$(($i+2))
        j=$(($j+2))
        k=$(($k+5))
        #routing
        routing='Resources/config/routing_rest.yml'
        lignes=`sed -n -e '2,7p' $routing`
        lignes="${lignes//PsClient/$fichier}"
        lignes="${lignes//psclient/${fichier,,}}"
        echo "$lignes" >> $routing
    done
}
convertion $*
