#!/bin/bash
convertion(){
    liste $1 $2
    remplacement $*
}
liste() {
    for file in `ls $1 |grep Ps`
    do
        echo ${file%%.*} >> $2
    done
}
remplacement() {
    dossierparent=$3
    echo $dossierparent
    fichier2=`cat $2`
    cd $3
    `rename 's/[[:blank:]]/_/g' *`
    for fichier in $fichier2
    do
        echo $fichier
        for file in  `find . -name 'PsClient*.php'`
        do
            cp $file ${file/'PsClient'/$fichier}
            sed -i "s/psclient/${fichier,,}/g" "${file/'PsClient'/$fichier}"
            sed -i "s/PsClient/$fichier/g" "${file/'PsClient'/$fichier}"
        done
    done
}
convertion $*
