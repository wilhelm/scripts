#!/bin/bash
convertion(){
    # on gère les arguments :
    #   arguments possibles :
    #       _-r : restaurer l'ancienne structure
    #       _-f : oublier les anciens fichiers
    REVERT=false
    SAVE=true
    while [[ $# -gt 1 ]]
    do
        key="$1"

        case $key in
            -r|--revert)
                REVERT=true
                shift # past argument
                ;;
            -f|--forget)
                SAVE=false
                shift # past argument
                ;;
            *)
                # unknown option
                shift
                ;;
        esac
    done
    cd $1
    `rename 's/[[:blank:]]/_/g' *`
    for element in `ls`
    do
        execPath=`pwd`
        #echo "element : $execPath/$element"
        if [ -d $element ] #si on a un dossier
        then #on convertit tout
            `rename 's/[[:blank:]]/_/g' *`
            echo "on a un dossier : $element "
            #echo "on va le traiter"
            cd $element
            execPath=`pwd`
            echo "appel récursif sur un sous-dossier : $execPath"
            s=""
            if [ $REVERT = true ]
            then
                s+=" -r"
            fi
            if [ $SAVE = false ]
            then
                s+=" -f"
            fi
            #echo $s
            convertion$s $execPath
        elif [ -f $element ] #si on a un fichier
        then
            if [ $REVERT = true ]
            then
                if [[ ${element##*.} == 'old' ]]
                then
                    echo "mv $element ${element%.*}"
                    mv $element ${element%.*}
                fi
            else
                echo "on a un fichier : $element"
                if [[ ${element#*.} =~ ^(php|css|html|js|java|c|cs|tex|bib|cpp|h|sh)$ ]]
                then
                    echo "extension : ${element#*.}"
                    cp $element $element'.old' #on sauvegarde une copie
                    sed -i 's/\t/    /g' $element #on change les tabulations en 4 espaces
                    sed -i 's/\s\+$//g' $element #on enlève les espaces à la fin des lignes
                    sed -i '/./,/^$/!d' $element #on supprime les sauts de ligne doubles
                    sed -i -e :a -e '/^\n*$/{$d;N;ba' -e '}' $element #on enlève les lignes vides à la fin

                    #séparer le traitement par extension
                    case ${element#*.} in
                        "php")
                            #gérer php
                            echo "On gère du PHP."
                        ;;
                        "html")
                            #gérer html
                            html "$element"
                        ;;
                        "css")
                            #gérer css
                            echo "On gère du CSS."
                        ;;
                        "js")
                            #gérer javascript
                            echo "On gère du JavaScript."
                        ;;
                        "java")
                            #gérer java
                            echo "On gère du Java."
                        ;;
                        "tex")
                            #gérer tex
                            echo "On gère du LateX."
                        ;;
                        "c"|"h")
                            #gérer C
                            echo "On gère du C."
                        ;;
                        "sh")
                            #gérer bash
                            echo "On gère du bash."
                        ;;
                        *)
                            echo "Ça, on gère pas encore"
                        ;;
                    esac
                fi
            fi
        fi
    done
    cd "../"
}
html() {
    sed 's/ \+/ /g' $1 | #enlever les espaces superflus
    sed 's/^ //' | #supprimer tous les espaces en début de ligne
    sed 's/ \?< \?\(\/\?\) \?/<\1/g' | sed 's/ \?> \?/>/g' | sed 's/ *\/ \?> \?/ \/>/g' | #supprimer les espaces indésirables dans les balises
    sed "s/\([^<]\+\)</\1\n</g" | #une balise commence sa ligne
    sed "s/>\([^$]\)/>\n\1/g" | #une balise finit sa ligne
    awk 'BEGIN{i=0; line="";b=0} {
        line=line$0
        #if(i>0) print balises[i-1]
        if(line ~ /<\!-- \(.*\)/) { # commentaire entier
            #printf "commentaire entier : "
        }
        else {
            if(0 != match(line, /<!--/,code)) { #début de commentaire
                #printf "début de commentaire : "
                balises[i]="-->"
                i++
                b=1
            }
            else {
                if(0 != match(line, /<([a-zA-Z0-9:]+) ?(.*) \/>/, attributs)) { #balise solitaire
                    #printf "balise solitaire : "
                }
                else {
                    if(0 != match(line, /<([a-zA-Z0-9:]+) ?(.*)>/, attributs)) { #début de balise
                        #printf "début de balise : "
                        balise = "</"attributs[1]">"
                        balises[i]=balise
                        i++
                        b=1
                    }
                    else {
                        if(line == balises[i-1]) { #fin de balise
                            #printf "fin de balise : "
                            i--
                            delete balises[i]
                        }
                        else {
                            if(line ~ /<[^>]*$/) { #balise pas finie
                                #print "balise pas finie : " line
                                next
                            }
                            else {
                                if(line ~ /<(.*)>/) { #balise fermante ne correspondant pas à une balise ouvrante
                                    #printf "balise fermante ne correspondant pas à une balise ouvrante : "
                                    if(i>0) {
                                        i--
                                    }
                                    # mettre un message derreur quelque part ?
                                }
                                else { #pas de balise
                                    #printf "pas de balise : "
                                }
                            }
                        }
                    }
                }
            }
        }
        printf b
        for(k=0; k < i-b; k++) {
            printf "    "
        }
        b=0
        print line
        line=""
    }'
}
convertion $*
