css() {
    sed 's/ \+/ /g' $1 | #enlever les espaces superflus
    sed 's/^ //' | #enlever tous les espaces en début de lignes
    sed 's/ *, */, /g' | #1 espace après les virgules
    sed 's/\([^ ]\) \?}/\1\n}/g' | #ajoute si nécessaire un saut de ligne avant }
    sed 's/\([{;}]\) \?\([^$]\)/\1\n\2/g' | #ajoute si besoin un saut de ligne après { ; et }
    sed 's/\/\*\([^ ]\)/\/\* \1/g' | #toujours un espace après /*
    sed 's/\([^ ]\)\*\//\1 \*\//g' | #toujours un espace avant */
    #fonction awk qui gère les commentaires
    awk 'BEGIN {i=0} {
        if( $0 ~ /^}/ ) {
            i--
        }
        for(k=0 ; k < i ; k++) {
            printf "  "
        }
        if( $0 ~ /{$/) {
            i++
        }
        print $0
    }' | #indentation
    sed 's/ *: *\(.*\);$/: \1;/' #: suivi d'un espace si sur une ligne d'un attribut
}
